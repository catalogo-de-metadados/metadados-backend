package com.api.metadadosbackend.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.api.metadadosbackend.dtos.EstatisticaDto;

public class EstatisticaMapper implements RowMapper<EstatisticaDto> {

	@Override
	public EstatisticaDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EstatisticaDto(rs.getLong(1), rs.getLong(2), rs.getString(3));
	}

}

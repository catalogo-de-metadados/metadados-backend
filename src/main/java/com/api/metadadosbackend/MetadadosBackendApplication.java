package com.api.metadadosbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetadadosBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetadadosBackendApplication.class, args);
	}

}

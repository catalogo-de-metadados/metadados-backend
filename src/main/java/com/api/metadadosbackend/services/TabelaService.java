package com.api.metadadosbackend.services;

import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.metadadosbackend.models.Esquema;
import com.api.metadadosbackend.models.Tabela;
import com.api.metadadosbackend.repositories.TabelaRepository;

@Service
public class TabelaService {

	final TabelaRepository tabelaRepository;

	public TabelaService(TabelaRepository tabelaRepository) {
		this.tabelaRepository = tabelaRepository;
	}

	@Transactional
	public Tabela save(Tabela tabela) {
		return tabelaRepository.save(tabela);
	}

	public boolean existsByNome(String nome) {
		return tabelaRepository.existsByNmTabela(nome);
	}

	public Page<Tabela> findAll(Pageable pageable) {
		return tabelaRepository.findAll(pageable);
	}

	public Page<Tabela> findAllByEsquemaId(UUID id, Pageable pageable) {
		return tabelaRepository.findAllTabelaByEsquemaId(id, pageable);
	}

	public Optional<Tabela> findByNomeEsquema(String nome, Esquema esquema) {
		return tabelaRepository.findByNmTabelaAndEsquema(nome, esquema);
	}

	public Optional<Tabela> findById(UUID id) {
		return tabelaRepository.findById(id);
	}

	@Transactional
	public void delete(Tabela tabela) {
		tabelaRepository.delete(tabela);
	}
}
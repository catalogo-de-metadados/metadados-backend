package com.api.metadadosbackend.services;

import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.metadadosbackend.models.Ambiente;
import com.api.metadadosbackend.repositories.AmbienteRepository;

@Service
public class AmbienteService {

	final AmbienteRepository ambienteRepository;

	public AmbienteService(AmbienteRepository ambienteRepository) {
		this.ambienteRepository = ambienteRepository;
	}

	public boolean existsByNmAmbiente(String nmAmbiente) {
		return ambienteRepository.existsByNmAmbiente(nmAmbiente);
	}

	@Transactional
	public Ambiente save(Ambiente ambiente) {
		return ambienteRepository.save(ambiente);
	}

	public Page<Ambiente> findAll(Pageable pageable) {
		return ambienteRepository.findAll(pageable);
	}

	public Optional<Ambiente> findById(UUID id) {
		return ambienteRepository.findById(id);
	}

	public void delete(Ambiente ambiente) {
		ambienteRepository.delete(ambiente);
	}
	
}

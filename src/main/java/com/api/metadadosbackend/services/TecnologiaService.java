package com.api.metadadosbackend.services;

import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.metadadosbackend.models.Tecnologia;
import com.api.metadadosbackend.repositories.TecnologiaRepository;

@Service
public class TecnologiaService {

	final TecnologiaRepository tecnologiaRepository;

	public TecnologiaService(TecnologiaRepository tecnologiaRepository) {
		this.tecnologiaRepository = tecnologiaRepository;
	}

	public boolean existsByNmTecnologia(String nmTecnologia) {
		return tecnologiaRepository.existsByNmTecnologia(nmTecnologia);
	}

	@Transactional
	public Tecnologia save(Tecnologia tecnologia) {
		return tecnologiaRepository.save(tecnologia);
	}

	public Page<Tecnologia> findAll(Pageable pageable) {
		return tecnologiaRepository.findAll(pageable);
	}

	public Optional<Tecnologia> findById(UUID id) {
		return tecnologiaRepository.findById(id);
	}

	public void delete(Tecnologia tecnologia) {
		tecnologiaRepository.delete(tecnologia);
	}
}

package com.api.metadadosbackend.services;

import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.metadadosbackend.models.Banco;
import com.api.metadadosbackend.models.Esquema;
import com.api.metadadosbackend.repositories.BancoRepository;
import com.api.metadadosbackend.repositories.EsquemaRepository;

@Service
public class EsquemaService {

	@Autowired
	BancoRepository bancoRepository;

	final EsquemaRepository esquemaRepository;

	public EsquemaService(EsquemaRepository esquemaRepository) {
		this.esquemaRepository = esquemaRepository;
	}

	@Transactional
	public Esquema save(Esquema esquema) {
		return esquemaRepository.save(esquema);
	}

	public boolean existsByNome(String nome) {
		return esquemaRepository.existsByNmEsquema(nome);
	}

	public Page<Esquema> findAll(Pageable pageable) {
		return esquemaRepository.findAll(pageable);
	}

	public Page<Esquema> findAllByBancoId(UUID id, Pageable pageable) {
		return esquemaRepository.findAllEsquemaByBancoId(id, pageable);
	}

	public Optional<Esquema> findByNomeBanco(String nome, Banco banco) {
		return esquemaRepository.findByNmEsquemaAndBanco(nome, banco);
	}

	public Optional<Esquema> findById(UUID id) {
		return esquemaRepository.findById(id);
	}

	@Transactional
	public void delete(Esquema esquema) {
		esquemaRepository.delete(esquema);
	}
}
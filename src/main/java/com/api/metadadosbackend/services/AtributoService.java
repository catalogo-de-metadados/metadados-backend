package com.api.metadadosbackend.services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.api.metadadosbackend.models.Atributo;
import com.api.metadadosbackend.models.Tabela;
import com.api.metadadosbackend.repositories.AtributoRepository;

@Service
public class AtributoService {

	final AtributoRepository atributoRepository;

	public AtributoService(AtributoRepository atributoRepository) {
		this.atributoRepository = atributoRepository;
	}

	@Transactional
	public Atributo save(Atributo atributo) {
		return atributoRepository.save(atributo);
	}

	public boolean existsByNome(String nome) {
		return atributoRepository.existsByNmAtributo(nome);
	}

	public Page<Atributo> findAll(Pageable pageable) {
		return atributoRepository.findAll(pageable);
	}

	public Page<Atributo> findAllByTabelaId(UUID id, Pageable pageable) {
		return atributoRepository.findAllAtributoByTabelaId(id, pageable);
	}

	public Optional<Atributo> findByNomeTabela(String nome, Tabela tabela) {
		return atributoRepository.findByNmAtributoAndTabela(nome, tabela);
	}

	public Optional<Atributo> findById(UUID id) {
		return atributoRepository.findById(id);
	}

	@Transactional
	public void delete(Atributo atributo) {
		atributoRepository.delete(atributo);
	}

	public List<Atributo> findAllAtributoPessoalSensivelByBancoId(UUID id) {
		return atributoRepository.findAllAtributoPessoalSensivelByBancoId(id);
	}

}
package com.api.metadadosbackend.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.api.metadadosbackend.dtos.EstatisticaDto;
import com.api.metadadosbackend.mapper.EstatisticaMapper;

@Service
@Transactional
public class EstatisticaService {

	@Autowired
	private JdbcTemplate jtm;

	public List<EstatisticaDto> obterQuantidades() {

		String sql = "SELECT 1, count(id), 'Bancos' FROM public.tb_banco UNION ";
		sql += "SELECT 2, count(id), 'Esquemas' FROM public.tb_esquema UNION ";
		sql += "SELECT 3, count(id), 'Tabelas' FROM public.tb_tabela UNION ";
		sql += "SELECT 4, count(id), 'Atributos' FROM public.tb_atributo";

		List<EstatisticaDto> lista = jtm.query(sql, new EstatisticaMapper());

		return lista;
	}

	public List<EstatisticaDto> obterClassificados() {

		String sql = "SELECT 1, count(*), 'Não classificados' ";
		sql += "FROM public.tb_atributo where in_pessoal is null and in_sensivel is null UNION ";
		sql += "SELECT 2, count(*), 'Classificados' ";
		sql += "FROM public.tb_atributo where in_pessoal is not null and in_sensivel is not null ";

		List<EstatisticaDto> lista = jtm.query(sql, new EstatisticaMapper());

		return lista;
	}

}

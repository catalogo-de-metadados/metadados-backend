package com.api.metadadosbackend.services;

import com.api.metadadosbackend.models.Etiqueta;
import com.api.metadadosbackend.repositories.EtiquetaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Service
public class EtiquetaService {

    final EtiquetaRepository etiquetaRepository;

    public EtiquetaService(EtiquetaRepository etiquetaRepository) {
        this.etiquetaRepository = etiquetaRepository;
    }

    public boolean existsByNmEtiqueta(String nmEtiqueta) {
        return etiquetaRepository.existsByNmEtiqueta(nmEtiqueta);
    }

    @Transactional
    public Etiqueta save(Etiqueta etiqueta) {
        return etiquetaRepository.save(etiqueta);
    }

    public Page<Etiqueta> findAll(Pageable pageable) {
        return etiquetaRepository.findAll(pageable);
    }

    public Optional<Etiqueta> findById(UUID id) {
        return etiquetaRepository.findById(id);
    }

    public void delete(Etiqueta etiqueta) {
        etiquetaRepository.delete(etiqueta);
    }
}

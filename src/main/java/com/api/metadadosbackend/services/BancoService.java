package com.api.metadadosbackend.services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.metadadosbackend.models.Ambiente;
import com.api.metadadosbackend.models.Banco;
import com.api.metadadosbackend.models.Tecnologia;
import com.api.metadadosbackend.repositories.BancoRepository;

@Service
public class BancoService {

	final BancoRepository bancoRepository;

	public BancoService(BancoRepository bancoRepository) {
		this.bancoRepository = bancoRepository;
	}

	@Transactional
	public Banco save(Banco banco) {
		return bancoRepository.save(banco);
	}

	public boolean existsByNome(String nome) {
		return bancoRepository.existsByNmBanco(nome);
	}

	public Page<Banco> findAll(Pageable pageable) {
		return bancoRepository.findAll(pageable);
	}

	public Optional<Banco> findById(UUID id) {
		return bancoRepository.findById(id);
	}

	public Optional<Banco> findByNomeAmbienteTecnologia(String nome, Ambiente ambiente, Tecnologia tecnologia) {
		return bancoRepository.findByNmBancoAndAmbienteAndTecnologia(nome, ambiente, tecnologia);
	}

	@Transactional
	public void delete(Banco banco) {
		bancoRepository.delete(banco);
	}
}

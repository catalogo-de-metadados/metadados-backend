package com.api.metadadosbackend.services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.metadadosbackend.models.Atributo;
import com.api.metadadosbackend.models.Contribuicao;
import com.api.metadadosbackend.repositories.ContribuicaoRepository;

@Service
public class ContribuicaoService {

	@Autowired
	ContribuicaoRepository contribuicaoRepository;

	@Transactional
	public Contribuicao save(Contribuicao contribuicao) {
		return contribuicaoRepository.save(contribuicao);
	}

	public List<Contribuicao> findAllByAtributoId(Atributo atributo) {
		return contribuicaoRepository.findByAtributo(atributo);
	}

	public Optional<Contribuicao> findById(UUID id) {
		return contribuicaoRepository.findById(id);
	}

	public void delete(Contribuicao contribuicao) {
		contribuicaoRepository.delete(contribuicao);
	}

}

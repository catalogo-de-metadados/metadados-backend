package com.api.metadadosbackend.controllers;

import com.api.metadadosbackend.dtos.AmbienteDto;
import com.api.metadadosbackend.dtos.ResponseMessage;
import com.api.metadadosbackend.models.Ambiente;
import com.api.metadadosbackend.services.AmbienteService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/ambiente")
public class AmbienteController {

	final AmbienteService ambienteService;

	public AmbienteController(AmbienteService ambienteService) {
		this.ambienteService = ambienteService;
	}

	@RolesAllowed("backend-admin")
	@PostMapping
	public ResponseEntity<Object> saveAmbiente(@RequestBody @Valid AmbienteDto ambienteDto) {
		if (ambienteService.existsByNmAmbiente(ambienteDto.getNmAmbiente())) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body("Este ambiente já está em uso.");
		}
		var ambiente = new Ambiente();
		BeanUtils.copyProperties(ambienteDto, ambiente);
		ambiente.setDtRegistro(LocalDateTime.now(ZoneId.of("UTC")));
		return ResponseEntity.status(HttpStatus.CREATED).body(ambienteService.save(ambiente));
	}

	@RolesAllowed("backend-admin")
	@GetMapping
	public ResponseEntity<Page<Ambiente>> getAllAmbientes(
			@PageableDefault(page = 0, size = 100, sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {
		return ResponseEntity.status(HttpStatus.OK).body(ambienteService.findAll(pageable));
	}

	@RolesAllowed("backend-admin")
	@GetMapping("/{id}")
	public ResponseEntity<Object> getAmbiente(@PathVariable(value = "id") UUID id) {
		Optional<Ambiente> ambienteOptional = ambienteService.findById(id);
		if (!ambienteOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Ambiente não encontrado.");
		}
		return ResponseEntity.status(HttpStatus.OK).body(ambienteOptional.get());
	}

	@RolesAllowed("backend-admin")
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteAmbiente(@PathVariable(value = "id") UUID id) {
		Optional<Ambiente> ambienteOptional = ambienteService.findById(id);
		if (!ambienteOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Ambiente não encontrado.");
		}
		try {
			ambienteService.delete(ambienteOptional.get());
		} catch (Exception e) {
			return new ResponseEntity<Object>(
					new ResponseMessage("Não é possível excluir este ambiente pois ele está associado a algum banco."),
					HttpStatus.CONFLICT);
		}
		return new ResponseEntity<Object>(new ResponseMessage("Ambiente excluído com sucesso."), HttpStatus.OK);
	}

	@RolesAllowed("backend-admin")
	@PutMapping("/{id}")
	public ResponseEntity<Object> updateAmbiente(@PathVariable(value = "id") UUID id,
			@RequestBody @Valid AmbienteDto ambienteDto) {
		Optional<Ambiente> ambienteOptional = ambienteService.findById(id);
		if (!ambienteOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Ambiente não encontrada.");
		}
		var ambiente = new Ambiente();
		BeanUtils.copyProperties(ambienteDto, ambiente);
		ambiente.setId(ambienteOptional.get().getId());
		ambiente.setDtRegistro(ambienteOptional.get().getDtRegistro());
		return ResponseEntity.status(HttpStatus.OK).body(ambienteService.save(ambiente));
	}
}

package com.api.metadadosbackend.controllers;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.metadadosbackend.dtos.BancoDto;
import com.api.metadadosbackend.dtos.ResponseMessage;
import com.api.metadadosbackend.models.Banco;
import com.api.metadadosbackend.models.Tabela;
import com.api.metadadosbackend.services.BancoService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/banco")
public class BancoController {

	final BancoService bancoService;

	public BancoController(BancoService bancoService) {
		this.bancoService = bancoService;
	}

	@RolesAllowed("backend-admin")
	@PostMapping
	public ResponseEntity<Object> saveBanco(@RequestBody @Valid BancoDto bancoDto) {
		Optional<Banco> bancoOptional = bancoService.findByNomeAmbienteTecnologia(bancoDto.getNmBanco(),
				bancoDto.getAmbiente(), bancoDto.getTecnologia());
		if (bancoOptional.isPresent()) {
			return new ResponseEntity<Object>(new ResponseMessage(
					"Não é permitido um banco que possua o mesmo nome, tecnologia e ambiente de um outro já cadastrado."),
					HttpStatus.CONFLICT);
		}
		var banco = new Banco();
		BeanUtils.copyProperties(bancoDto, banco);
		banco.setDtRegistro(LocalDateTime.now(ZoneId.of("UTC")));
		return ResponseEntity.status(HttpStatus.CREATED).body(bancoService.save(banco));
	}

	@RolesAllowed("backend-user")
	@GetMapping
	public ResponseEntity<Page<Banco>> getAllBancos(
			@PageableDefault(page = 0, size = 100, sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {
		return ResponseEntity.status(HttpStatus.OK).body(bancoService.findAll(pageable));
	}

	@RolesAllowed("backend-user")
	@GetMapping("/{id}")
	public ResponseEntity<Object> getBanco(@PathVariable(value = "id") UUID id) {
		Optional<Banco> bancoOptional = bancoService.findById(id);
		if (!bancoOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Banco não encontrado.");
		}
		return ResponseEntity.status(HttpStatus.OK).body(bancoOptional.get());
	}

	@RolesAllowed("backend-admin")
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteBanco(@PathVariable(value = "id") UUID id) {
		Optional<Banco> bancoOptional = bancoService.findById(id);
		if (!bancoOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Banco não encontrado.");
		}
		try {
			bancoService.delete(bancoOptional.get());
		} catch (Exception e) {
			return new ResponseEntity<Object>(
					new ResponseMessage(
							"Para excluir este banco é necessário excluir primeiro os esquemas associados."),
					HttpStatus.CONFLICT);
		}
		return new ResponseEntity<Object>(new ResponseMessage("Banco excluído com sucesso."), HttpStatus.OK);
	}

	@RolesAllowed("backend-admin")
	@PutMapping("/{id}")
	public ResponseEntity<Object> updateBanco(@PathVariable(value = "id") UUID id,
			@RequestBody @Valid BancoDto bancoDto) {

		Optional<Banco> bancoOptional = bancoService.findById(id);
		if (!bancoOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Banco não encontrado.");
		}
		Optional<Banco> nomeAmbienteTecnologiaOptional = bancoService
				.findByNomeAmbienteTecnologia(bancoDto.getNmBanco(), bancoDto.getAmbiente(), bancoDto.getTecnologia());
		if (nomeAmbienteTecnologiaOptional.isPresent() && !nomeAmbienteTecnologiaOptional.get().getId().equals(id)) {

			return new ResponseEntity<Object>(new ResponseMessage(
					"Não é permitido um banco que possua o mesmo nome, tecnologia e ambiente de um outro já cadastrado."),
					HttpStatus.CONFLICT);
		}
		var banco = new Banco();
		BeanUtils.copyProperties(bancoDto, banco);
		banco.setId(bancoOptional.get().getId());
		banco.setDtRegistro(bancoOptional.get().getDtRegistro());
		return ResponseEntity.status(HttpStatus.OK).body(bancoService.save(banco));
	}
}

package com.api.metadadosbackend.controllers;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.metadadosbackend.dtos.EtiquetaDto;
import com.api.metadadosbackend.dtos.ResponseMessage;
import com.api.metadadosbackend.models.Etiqueta;
import com.api.metadadosbackend.services.EtiquetaService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/etiqueta")
public class EtiquetaController {

	final EtiquetaService etiquetaService;

	public EtiquetaController(EtiquetaService etiquetaService) {
		this.etiquetaService = etiquetaService;
	}

	@RolesAllowed("backend-admin")
	@PostMapping
	public ResponseEntity<Object> saveEtiqueta(@RequestBody @Valid EtiquetaDto etiquetaDto) {
		if (etiquetaService.existsByNmEtiqueta(etiquetaDto.getNmEtiqueta())) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body("Esta etiqueta já está em uso!");
		}
		var etiqueta = new Etiqueta();
		BeanUtils.copyProperties(etiquetaDto, etiqueta);
		etiqueta.setDtRegistro(LocalDateTime.now(ZoneId.of("UTC")));
		return ResponseEntity.status(HttpStatus.CREATED).body(etiquetaService.save(etiqueta));
	}

	@RolesAllowed("backend-admin")
	@GetMapping
	public ResponseEntity<Page<Etiqueta>> getAllEtiquetas(
			@PageableDefault(page = 0, size = 100, sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {
		return ResponseEntity.status(HttpStatus.OK).body(etiquetaService.findAll(pageable));
	}

	@RolesAllowed("backend-admin")
	@GetMapping("/{id}")
	public ResponseEntity<Object> getEtiqueta(@PathVariable(value = "id") UUID id) {
		Optional<Etiqueta> etiquetaOptional = etiquetaService.findById(id);
		if (!etiquetaOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Etiqueta não encontrada.");
		}
		return ResponseEntity.status(HttpStatus.OK).body(etiquetaOptional.get());
	}

	@RolesAllowed("backend-admin")
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteEtiqueta(@PathVariable(value = "id") UUID id) {
		Optional<Etiqueta> etiquetaOptional = etiquetaService.findById(id);
		if (!etiquetaOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Etiqueta não encontrada.");
		}
		try {
			etiquetaService.delete(etiquetaOptional.get());
		} catch (Exception e) {
			return new ResponseEntity<Object>(new ResponseMessage(
					"Não é possível excluir esta etiqueta pois ela está sendo utilizada na classificação LGPD de algum atributo."),
					HttpStatus.CONFLICT);
		}
		return new ResponseEntity<Object>(new ResponseMessage("Etiqueta excluída com sucesso."), HttpStatus.OK);

	}

	@RolesAllowed("backend-admin")
	@PutMapping("/{id}")
	public ResponseEntity<Object> updateEtiqueta(@PathVariable(value = "id") UUID id,
			@RequestBody @Valid EtiquetaDto etiquetaDto) {
		Optional<Etiqueta> etiquetaOptional = etiquetaService.findById(id);
		if (!etiquetaOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Etiqueta não encontrada.");
		}
		var etiqueta = new Etiqueta();
		BeanUtils.copyProperties(etiquetaDto, etiqueta);
		etiqueta.setId(etiquetaOptional.get().getId());
		etiqueta.setDtRegistro(etiquetaOptional.get().getDtRegistro());
		return ResponseEntity.status(HttpStatus.OK).body(etiquetaService.save(etiqueta));
	}
}

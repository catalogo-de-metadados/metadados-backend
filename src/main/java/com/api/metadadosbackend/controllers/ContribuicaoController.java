package com.api.metadadosbackend.controllers;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.metadadosbackend.dtos.ContribuicaoDto;
import com.api.metadadosbackend.dtos.ResponseMessage;
import com.api.metadadosbackend.models.Ambiente;
import com.api.metadadosbackend.models.Atributo;
import com.api.metadadosbackend.models.Contribuicao;
import com.api.metadadosbackend.services.AtributoService;
import com.api.metadadosbackend.services.ContribuicaoService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/contribuicao")
public class ContribuicaoController {
	

	@Autowired
	ContribuicaoService contribuicaoService;

	@Autowired
	AtributoService atributoService;
	
	@RolesAllowed("backend-user")
	@PostMapping
	public ResponseEntity<Object> saveContribuicao(@RequestBody @Valid ContribuicaoDto contribuicaoDto) {
		var contribuicao = new Contribuicao();
		BeanUtils.copyProperties(contribuicaoDto, contribuicao);
		contribuicao.setDtRegistro(LocalDateTime.now(ZoneId.of("UTC")));
		return ResponseEntity.status(HttpStatus.CREATED).body(contribuicaoService.save(contribuicao));
	}

	@RolesAllowed("backend-user")
	@GetMapping("/getAllContribuicoesByAtributoId/{id}")
	public ResponseEntity<List<Contribuicao>> getAllContribuicoesByAtributoId(@PathVariable("id") UUID id) {
		Optional<Atributo> atributoOptional = atributoService.findById(id);
		return ResponseEntity.status(HttpStatus.OK).body(contribuicaoService.findAllByAtributoId(atributoOptional.get()));
	}
	
	@RolesAllowed("backend-user")
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteAmbiente(@PathVariable(value = "id") UUID id) {
		Optional<Contribuicao> contribuicaoOptional = contribuicaoService.findById(id);
		if (!contribuicaoOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Contribuição não encontrada.");
		}
		contribuicaoService.delete(contribuicaoOptional.get());
		return new ResponseEntity<Object>(new ResponseMessage("Contribuição excluída com sucesso."), HttpStatus.OK);
	}

}

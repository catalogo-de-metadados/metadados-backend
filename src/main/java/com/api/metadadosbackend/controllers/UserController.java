package com.api.metadadosbackend.controllers;

import com.api.metadadosbackend.dtos.ResponseMessage;
import com.api.metadadosbackend.models.User;
import com.api.metadadosbackend.services.KeycloakService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@Api( tags = "Usuário")
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    private KeycloakService keycloakService;

    @PostMapping("/create")
    public ResponseEntity<ResponseMessage> create(@RequestBody User user){
        Object[] obj = keycloakService.createUser(user);
        int status = (int) obj[0];
        ResponseMessage message = (ResponseMessage) obj[1];
        return ResponseEntity.status(status).body(message);
    }
}
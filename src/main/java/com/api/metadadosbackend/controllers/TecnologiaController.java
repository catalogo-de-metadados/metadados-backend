package com.api.metadadosbackend.controllers;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.metadadosbackend.dtos.ResponseMessage;
import com.api.metadadosbackend.dtos.TecnologiaDto;
import com.api.metadadosbackend.models.Tecnologia;
import com.api.metadadosbackend.services.TecnologiaService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/tecnologia")
public class TecnologiaController {

	final TecnologiaService tecnologiaService;

	public TecnologiaController(TecnologiaService tecnologiaService) {
		this.tecnologiaService = tecnologiaService;
	}

	@RolesAllowed("backend-admin")
	@PostMapping
	public ResponseEntity<Object> saveTecnologia(@RequestBody @Valid TecnologiaDto tecnologiaDto) {
		if (tecnologiaService.existsByNmTecnologia(tecnologiaDto.getNmTecnologia())) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body("Esta tecnologia já está em uso!");
		}
		var tecnologia = new Tecnologia();
		BeanUtils.copyProperties(tecnologiaDto, tecnologia);
		tecnologia.setDtRegistro(LocalDateTime.now(ZoneId.of("UTC")));
		return ResponseEntity.status(HttpStatus.CREATED).body(tecnologiaService.save(tecnologia));
	}

	@RolesAllowed("backend-admin")
	@GetMapping
	public ResponseEntity<Page<Tecnologia>> getAllTecnologias(
			@PageableDefault(page = 0, size = 100, sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {
		return ResponseEntity.status(HttpStatus.OK).body(tecnologiaService.findAll(pageable));
	}

	@RolesAllowed("backend-admin")
	@GetMapping("/{id}")
	public ResponseEntity<Object> getTecnologia(@PathVariable(value = "id") UUID id) {
		Optional<Tecnologia> tecnologiaOptional = tecnologiaService.findById(id);
		if (!tecnologiaOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Tecnologia não encontrada.");
		}
		return ResponseEntity.status(HttpStatus.OK).body(tecnologiaOptional.get());
	}

	@RolesAllowed("backend-admin")
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteTecnologia(@PathVariable(value = "id") UUID id) {
		Optional<Tecnologia> tecnologiaOptional = tecnologiaService.findById(id);
		if (!tecnologiaOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Tecnologia não encontrada.");
		}

		try {
			tecnologiaService.delete(tecnologiaOptional.get());
		} catch (Exception e) {
			return new ResponseEntity<Object>(
					new ResponseMessage(
							"Não é possível excluir esta tecnologia pois ela está associada a algum banco."),
					HttpStatus.CONFLICT);
		}

		return new ResponseEntity<Object>(new ResponseMessage("Tecnologia excluída com sucesso."), HttpStatus.OK);

	}

	@RolesAllowed("backend-admin")
	@PutMapping("/{id}")
	public ResponseEntity<Object> updateTecnologia(@PathVariable(value = "id") UUID id,
			@RequestBody @Valid TecnologiaDto tecnologiaDto) {
		Optional<Tecnologia> tecnologiaOptional = tecnologiaService.findById(id);
		if (!tecnologiaOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Tecnologia não encontrada.");
		}
		var tecnologia = new Tecnologia();
		BeanUtils.copyProperties(tecnologiaDto, tecnologia);
		tecnologia.setId(tecnologiaOptional.get().getId());
		tecnologia.setDtRegistro(tecnologiaOptional.get().getDtRegistro());
		return ResponseEntity.status(HttpStatus.OK).body(tecnologiaService.save(tecnologia));
	}
}

package com.api.metadadosbackend.controllers;

import java.util.List;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.metadadosbackend.dtos.EstatisticaDto;
import com.api.metadadosbackend.services.EstatisticaService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/estatistica")
public class EstatisticaController {

	@Autowired
	EstatisticaService estatisticaService;

	@RolesAllowed("backend-user")
	@GetMapping("/obterQuantidades")
	public ResponseEntity<List<EstatisticaDto>> obterQuantidades() {
		return ResponseEntity.status(HttpStatus.OK).body(estatisticaService.obterQuantidades());
	}

	@RolesAllowed("backend-user")
	@GetMapping("/obterClassificados")
	public ResponseEntity<List<EstatisticaDto>> obterClassificados() {
		return ResponseEntity.status(HttpStatus.OK).body(estatisticaService.obterClassificados());
	}

}

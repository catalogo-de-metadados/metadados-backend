package com.api.metadadosbackend.controllers;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.metadadosbackend.dtos.AtributoDto;
import com.api.metadadosbackend.dtos.ResponseMessage;
import com.api.metadadosbackend.models.Atributo;
import com.api.metadadosbackend.services.AtributoService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/atributo")
public class AtributoController {

	final AtributoService atributoService;

	public AtributoController(AtributoService atributoService) {
		this.atributoService = atributoService;
	}

	@RolesAllowed("backend-admin")
	@PostMapping
	public ResponseEntity<Object> saveAtributo(@RequestBody @Valid AtributoDto atributoDto) {
		Optional<Atributo> atributoOptional = atributoService.findByNomeTabela(atributoDto.getNmAtributo(),
				atributoDto.getTabela());
		if (atributoOptional.isPresent()) {
			return new ResponseEntity<Object>(new ResponseMessage(
					"Não é permitido um atributo que tenha o mesmo nome de um outro já cadastrado nesta tabela."),
					HttpStatus.CONFLICT);
		}
		var atributo = new Atributo();
		BeanUtils.copyProperties(atributoDto, atributo);
		atributo.setDtRegistro(LocalDateTime.now(ZoneId.of("UTC")));
		return ResponseEntity.status(HttpStatus.CREATED).body(atributoService.save(atributo));
	}

	@RolesAllowed("backend-user")
	@GetMapping
	public ResponseEntity<Page<Atributo>> getAllAtributos(
			@PageableDefault(page = 0, size = 100, sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {
		return ResponseEntity.status(HttpStatus.OK).body(atributoService.findAll(pageable));
	}

	@RolesAllowed("backend-user")
	@GetMapping("/getAllAtributosByTabelaId/{id}")
	public ResponseEntity<Page<Atributo>> getAllAtributosByTabelaId(@PathVariable("id") UUID id,
			@PageableDefault(page = 0, size = 100, sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {
		return ResponseEntity.status(HttpStatus.OK).body(atributoService.findAllByTabelaId(id, pageable));
	}

	@RolesAllowed("backend-user")
	@GetMapping("/getAllAtributoPessoalSensivelByBancoId/{id}")
	public ResponseEntity<List<Atributo>> getAllAtributoPessoalSensivelByBancoId(@PathVariable("id") UUID id) {
		return ResponseEntity.status(HttpStatus.OK).body(atributoService.findAllAtributoPessoalSensivelByBancoId(id));
	}

	@RolesAllowed("backend-user")
	@GetMapping("/{id}")
	public ResponseEntity<Object> getAtributo(@PathVariable(value = "id") UUID id) {
		Optional<Atributo> atributoOptional = atributoService.findById(id);
		if (!atributoOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Atributo não encontrado.");
		}
		return ResponseEntity.status(HttpStatus.OK).body(atributoOptional.get());
	}

	@RolesAllowed("backend-admin")
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteAtributo(@PathVariable(value = "id") UUID id) {
		Optional<Atributo> atributoOptional = atributoService.findById(id);
		if (!atributoOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Atributo não encontrado.");
		}
		atributoService.delete(atributoOptional.get());
		return new ResponseEntity<Object>(new ResponseMessage("Atributo excluído com sucesso."), HttpStatus.OK);
	}

	@RolesAllowed("backend-admin")
	@PutMapping("/{id}")
	public ResponseEntity<Object> updateAtributo(@PathVariable(value = "id") UUID id,
			@RequestBody @Valid AtributoDto atributoDto) {

		Optional<Atributo> atributoOptional = atributoService.findById(id);
		if (!atributoOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Atributo não encontrado.");
		}

		Optional<Atributo> nomeTabelaOptional = atributoService.findByNomeTabela(atributoDto.getNmAtributo(),
				atributoDto.getTabela());
		if (nomeTabelaOptional.isPresent() && !nomeTabelaOptional.get().getId().equals(id)) {
			return new ResponseEntity<Object>(new ResponseMessage(
					"Não é permitido um atributo que tenha o mesmo nome de um outro já cadastrado nesta tabela."),
					HttpStatus.CONFLICT);
		}

		var atributo = new Atributo();
		BeanUtils.copyProperties(atributoDto, atributo);
		atributo.setId(atributoOptional.get().getId());
		atributo.setDtRegistro(atributoOptional.get().getDtRegistro());
		return ResponseEntity.status(HttpStatus.OK).body(atributoService.save(atributo));
	}
}

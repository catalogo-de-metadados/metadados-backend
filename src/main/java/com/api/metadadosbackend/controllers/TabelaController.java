package com.api.metadadosbackend.controllers;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.metadadosbackend.dtos.ResponseMessage;
import com.api.metadadosbackend.dtos.TabelaDto;
import com.api.metadadosbackend.models.Esquema;
import com.api.metadadosbackend.models.Tabela;
import com.api.metadadosbackend.services.TabelaService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/tabela")
public class TabelaController {

	final TabelaService tabelaService;

	public TabelaController(TabelaService tabelaService) {
		this.tabelaService = tabelaService;
	}

	@RolesAllowed("backend-admin")
	@PostMapping
	public ResponseEntity<Object> saveTabela(@RequestBody @Valid TabelaDto tabelaDto) {
		Optional<Tabela> tabelaOptional = tabelaService.findByNomeEsquema(tabelaDto.getNmTabela(),
				tabelaDto.getEsquema());
		if (tabelaOptional.isPresent()) {
			return new ResponseEntity<Object>(new ResponseMessage(
					"Não é permitida um tabela que tenha o mesmo nome de uma outra já cadastrada nesta tabela."),
					HttpStatus.CONFLICT);
		}
		var tabela = new Tabela();
		BeanUtils.copyProperties(tabelaDto, tabela);
		tabela.setDtRegistro(LocalDateTime.now(ZoneId.of("UTC")));
		return ResponseEntity.status(HttpStatus.CREATED).body(tabelaService.save(tabela));
	}

	@RolesAllowed("backend-user")
	@GetMapping
	public ResponseEntity<Page<Tabela>> getAllTabelas(
			@PageableDefault(page = 0, size = 100, sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {
		return ResponseEntity.status(HttpStatus.OK).body(tabelaService.findAll(pageable));
	}

	@RolesAllowed("backend-user")
	@GetMapping("/getAllTabelasByEsquemaId/{id}")
	public ResponseEntity<Page<Tabela>> getAllEsquemasByBancoId(@PathVariable("id") UUID id,
			@PageableDefault(page = 0, size = 100, sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {
		return ResponseEntity.status(HttpStatus.OK).body(tabelaService.findAllByEsquemaId(id, pageable));
	}

	@RolesAllowed("backend-user")
	@GetMapping("/{id}")
	public ResponseEntity<Object> getTabela(@PathVariable(value = "id") UUID id) {
		Optional<Tabela> tabelaOptional = tabelaService.findById(id);
		if (!tabelaOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Tabela não encontrada.");
		}
		return ResponseEntity.status(HttpStatus.OK).body(tabelaOptional.get());
	}

	@RolesAllowed("backend-admin")
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteTabela(@PathVariable(value = "id") UUID id) {
		Optional<Tabela> tabelaOptional = tabelaService.findById(id);
		if (!tabelaOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Tabela não encontrada.");
		}
		try {
			tabelaService.delete(tabelaOptional.get());
		} catch (Exception e) {
			return new ResponseEntity<Object>(
					new ResponseMessage(
							"Para excluir esta tabela é necessário excluir primeiro os atributos associados."),
					HttpStatus.CONFLICT);
		}
		return new ResponseEntity<Object>(new ResponseMessage("Tabela excluída com sucesso."), HttpStatus.OK);

	}

	@RolesAllowed("backend-admin")
	@PutMapping("/{id}")
	public ResponseEntity<Object> updateTabela(@PathVariable(value = "id") UUID id,
			@RequestBody @Valid TabelaDto tabelaDto) {

		Optional<Tabela> tabelaOptional = tabelaService.findById(id);
		if (!tabelaOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Tabela não encontrada.");
		}

		Optional<Tabela> nomeTabelaOptional = tabelaService.findByNomeEsquema(tabelaDto.getNmTabela(),
				tabelaDto.getEsquema());
		if (nomeTabelaOptional.isPresent() && !nomeTabelaOptional.get().getId().equals(id)) {
			return new ResponseEntity<Object>(new ResponseMessage(
					"Não é permitida um tabela que tenha o mesmo nome de uma outra já cadastrada nesta tabela."),
					HttpStatus.CONFLICT);
		}
		var tabela = new Tabela();
		BeanUtils.copyProperties(tabelaDto, tabela);
		tabela.setId(tabelaOptional.get().getId());
		tabela.setDtRegistro(tabelaOptional.get().getDtRegistro());
		return ResponseEntity.status(HttpStatus.OK).body(tabelaService.save(tabela));
	}
}

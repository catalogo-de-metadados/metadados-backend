package com.api.metadadosbackend.controllers;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.metadadosbackend.dtos.EsquemaDto;
import com.api.metadadosbackend.dtos.ResponseMessage;
import com.api.metadadosbackend.models.Atributo;
import com.api.metadadosbackend.models.Esquema;
import com.api.metadadosbackend.services.EsquemaService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/esquema")
public class EsquemaController {

	final EsquemaService esquemaService;

	public EsquemaController(EsquemaService esquemaService) {
		this.esquemaService = esquemaService;
	}

	@RolesAllowed("backend-admin")
	@PostMapping
	public ResponseEntity<Object> saveEsquema(@RequestBody @Valid EsquemaDto esquemaDto) {
		Optional<Esquema> esquemaOptional = esquemaService.findByNomeBanco(esquemaDto.getNmEsquema(),
				esquemaDto.getBanco());
		if (esquemaOptional.isPresent()) {
			return new ResponseEntity<Object>(
					new ResponseMessage(
							"Não é permitido um esquema que tenha o mesmo nome de um outro já cadastrado neste banco."),
					HttpStatus.CONFLICT);
		}

		var esquema = new Esquema();
		BeanUtils.copyProperties(esquemaDto, esquema);
		esquema.setDtRegistro(LocalDateTime.now(ZoneId.of("UTC")));
		return ResponseEntity.status(HttpStatus.CREATED).body(esquemaService.save(esquema));
	}

	@RolesAllowed("backend-user")
	@GetMapping
	public ResponseEntity<Page<Esquema>> getAllEsquemas(
			@PageableDefault(page = 0, size = 100, sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {
		return ResponseEntity.status(HttpStatus.OK).body(esquemaService.findAll(pageable));
	}

	@RolesAllowed("backend-user")
	@GetMapping("/getAllEsquemasByBancoId/{id}")
	public ResponseEntity<Page<Esquema>> getAllEsquemasByBancoId(@PathVariable("id") UUID id,
			@PageableDefault(page = 0, size = 100, sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {
		return ResponseEntity.status(HttpStatus.OK).body(esquemaService.findAllByBancoId(id, pageable));
	}

	@RolesAllowed("backend-user")
	@GetMapping("/{id}")
	public ResponseEntity<Object> getEsquema(@PathVariable(value = "id") UUID id) {
		Optional<Esquema> esquemaOptional = esquemaService.findById(id);
		if (!esquemaOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Esquema não encontrado.");
		}
		return ResponseEntity.status(HttpStatus.OK).body(esquemaOptional.get());
	}

	@RolesAllowed("backend-admin")
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteEsquema(@PathVariable(value = "id") UUID id) {
		Optional<Esquema> esquemaOptional = esquemaService.findById(id);
		if (!esquemaOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Esquema não encontrado.");
		}
		try {
			esquemaService.delete(esquemaOptional.get());
		} catch (Exception e) {
			return new ResponseEntity<Object>(
					new ResponseMessage(
							"Para excluir este esquema é necessário excluir primeiro as tabelas associadas."),
					HttpStatus.CONFLICT);
		}
		return new ResponseEntity<Object>(new ResponseMessage("Esquema excluído com sucesso."), HttpStatus.OK);

	}

	@RolesAllowed("backend-admin")
	@PutMapping("/{id}")
	public ResponseEntity<Object> updateEsquema(@PathVariable(value = "id") UUID id,
			@RequestBody @Valid EsquemaDto esquemaDto) {

		Optional<Esquema> esquemaOptional = esquemaService.findById(id);
		if (!esquemaOptional.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Esquema não encontrado.");
		}

		Optional<Esquema> nomeBancoOptional = esquemaService.findByNomeBanco(esquemaDto.getNmEsquema(),
				esquemaDto.getBanco());
		if (nomeBancoOptional.isPresent() && !nomeBancoOptional.get().getId().equals(id)) {
			return new ResponseEntity<Object>(
					new ResponseMessage(
							"Não é permitido um esquema que tenha o mesmo nome de um outro já cadastrado neste banco."),
					HttpStatus.CONFLICT);
		}
		var esquema = new Esquema();
		BeanUtils.copyProperties(esquemaDto, esquema);
		esquema.setId(esquemaOptional.get().getId());
		esquema.setDtRegistro(esquemaOptional.get().getDtRegistro());
		return ResponseEntity.status(HttpStatus.OK).body(esquemaService.save(esquema));
	}
}

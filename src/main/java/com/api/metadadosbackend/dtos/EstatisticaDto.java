package com.api.metadadosbackend.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Data
public class EstatisticaDto {
	private static final long serialVersionUID = 1L;
	private Long x;
	private Long y;
	private String label;
}
package com.api.metadadosbackend.dtos;

import javax.validation.constraints.NotBlank;

import com.api.metadadosbackend.models.Atributo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContribuicaoDto {

	@NotBlank
	private String txContribuicao;

	@NotBlank
	private String usrContribuicao;
	
	private Atributo atributo;

}

package com.api.metadadosbackend.dtos;

import javax.validation.constraints.NotBlank;

import com.api.metadadosbackend.models.Ambiente;
import com.api.metadadosbackend.models.Tecnologia;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BancoDto {

	@NotBlank
	private String nmBanco;
	private String nmAmbiente;
	private String nmTecnologia;
	private String txDescricao;
	private Ambiente ambiente;
	private Tecnologia tecnologia;

}

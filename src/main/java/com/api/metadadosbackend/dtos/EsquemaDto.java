package com.api.metadadosbackend.dtos;

import java.util.UUID;

import javax.validation.constraints.NotBlank;

import com.api.metadadosbackend.models.Banco;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EsquemaDto {

	@NotBlank
	private String nmEsquema;
	private String txDescricao;
//	private List<Tabela> tabelas = new ArrayList<>();
	private Banco banco;

}

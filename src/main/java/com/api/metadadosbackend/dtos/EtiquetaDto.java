package com.api.metadadosbackend.dtos;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EtiquetaDto {

	@NotBlank
	private String nmEtiqueta;
	private String inTipoEtiqueta;

}

package com.api.metadadosbackend.dtos;

import java.util.List;

import javax.validation.constraints.NotBlank;

import com.api.metadadosbackend.models.Contribuicao;
import com.api.metadadosbackend.models.Etiqueta;
import com.api.metadadosbackend.models.Tabela;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AtributoDto {

	@NotBlank
	private String nmAtributo;
	private String inSensivel;
	private String inPessoal;
	private String tipo;
	private Double tamanho;
	private Boolean inObrigatorio;
	private Boolean inCampoChave;
	private Boolean inCampoFk;
	private String txDescricao;
	private List<Etiqueta> etiqueta;
//	private List<Contribuicao> contribuicao;
	private Tabela tabela;
}

package com.api.metadadosbackend.dtos;

import javax.validation.constraints.NotBlank;

import com.api.metadadosbackend.models.Esquema;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TabelaDto {

	@NotBlank
	private String nmTabela;
	private String txDescricao;
	private Esquema esquema;

}

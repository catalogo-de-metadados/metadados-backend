package com.api.metadadosbackend.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.metadadosbackend.models.Ambiente;

@Repository
public interface AmbienteRepository extends JpaRepository<Ambiente, UUID>  {
	boolean existsByNmAmbiente(String nome);

}

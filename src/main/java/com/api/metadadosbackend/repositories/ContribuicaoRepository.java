package com.api.metadadosbackend.repositories;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.metadadosbackend.models.Atributo;
import com.api.metadadosbackend.models.Contribuicao;

@Repository

public interface ContribuicaoRepository extends JpaRepository<Contribuicao, UUID> {

	List<Contribuicao> findByAtributo(Atributo atributo);
}

package com.api.metadadosbackend.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.metadadosbackend.models.Esquema;
import com.api.metadadosbackend.models.Tabela;

@Repository
public interface TabelaRepository extends JpaRepository<Tabela, UUID> {

	boolean existsByNmTabela(String nome);

	Page<Tabela> findAllTabelaByEsquemaId(UUID id, Pageable pageable);

	Optional<Tabela> findByNmTabelaAndEsquema(String nmTabela, Esquema esquema);

}

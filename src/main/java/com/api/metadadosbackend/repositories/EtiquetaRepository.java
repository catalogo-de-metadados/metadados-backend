package com.api.metadadosbackend.repositories;

import com.api.metadadosbackend.models.Etiqueta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface EtiquetaRepository extends JpaRepository<Etiqueta, UUID> {
    boolean existsByNmEtiqueta(String nome);


}

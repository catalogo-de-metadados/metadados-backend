package com.api.metadadosbackend.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.metadadosbackend.models.Banco;
import com.api.metadadosbackend.models.Esquema;

@Repository
public interface EsquemaRepository extends JpaRepository<Esquema, UUID> {
	boolean existsByNmEsquema(String nome);

	Page<Esquema> findAllEsquemaByBancoId(UUID id, Pageable pageable);

	Optional<Esquema> findByNmEsquemaAndBanco(String nmEsquema, Banco banco);

}

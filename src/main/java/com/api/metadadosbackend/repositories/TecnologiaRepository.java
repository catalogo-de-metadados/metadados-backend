package com.api.metadadosbackend.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.metadadosbackend.models.Tecnologia;

@Repository
public interface TecnologiaRepository extends JpaRepository<Tecnologia, UUID> {
	boolean existsByNmTecnologia(String nome);

}

package com.api.metadadosbackend.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.api.metadadosbackend.models.Atributo;
import com.api.metadadosbackend.models.Tabela;

@Repository
public interface AtributoRepository extends JpaRepository<Atributo, UUID> {
	boolean existsByNmAtributo(String nome);

	Page<Atributo> findAllAtributoByTabelaId(UUID id, Pageable pageable);

	Optional<Atributo> findByNmAtributoAndTabela(String nmAtributo, Tabela tabela);

	@Query(value = "select * from tb_atributo  \r\n"
			+ "inner JOIN tb_tabela on tb_atributo.tabela_id = tb_tabela.id \r\n"
			+ "inner JOIN tb_esquema on tb_tabela.esquema_id = tb_esquema.id \r\n" + "where banco_id = ?1 \r\n"
			+ "and (in_pessoal = 'S' or in_sensivel = 'S')", nativeQuery = true)
	List<Atributo> findAllAtributoPessoalSensivelByBancoId(UUID id);

}

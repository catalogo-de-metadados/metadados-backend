package com.api.metadadosbackend.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.metadadosbackend.models.Ambiente;
import com.api.metadadosbackend.models.Banco;
import com.api.metadadosbackend.models.Tecnologia;

@Repository
public interface BancoRepository extends JpaRepository<Banco, UUID> {

	boolean existsByNmBanco(String nome);

	Optional<Banco> findByNmBancoAndAmbienteAndTecnologia(String nmBanco, Ambiente ambiente, Tecnologia tecnologia);

}

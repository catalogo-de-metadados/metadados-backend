package com.api.metadadosbackend.models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_BANCO")
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Banco implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Setter
	@Getter
	private UUID id;

	@Column(nullable = false, length = 120)
	@Getter
	@Setter
	private String nmBanco;

	@Column
	@Getter
	@Setter
	private String txDescricao;

	@Column(nullable = false)
	@Getter
	@Setter
	private LocalDateTime dtRegistro;

	@ManyToOne
	@Getter
	@Setter
	private Ambiente ambiente;

	@ManyToOne
	@Getter
	@Setter
	private Tecnologia tecnologia;

//	@OneToMany(mappedBy = "banco", targetEntity = Esquema.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//	@Getter
//	@Setter
//	@JsonIgnore
//	private List<Esquema> esquemas;

}

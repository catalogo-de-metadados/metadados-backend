package com.api.metadadosbackend.models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_ATRIBUTO")
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Atributo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Setter
	@Getter
	private UUID id;

	@Column(nullable = false, length = 120)
	@Getter
	@Setter
	private String nmAtributo;

	@Column(length = 1)
	@Getter
	@Setter
	private String inSensivel;

	@Column(length = 1)
	@Getter
	@Setter
	private String inPessoal;

	@Column
	@Getter
	@Setter
	private String tipo;

	@Column
	@Getter
	@Setter
	private Double tamanho;

	@Column
	@Getter
	@Setter
	private Boolean inObrigatorio;

	@Column
	@Getter
	@Setter
	private Boolean inCampoChave;

	@Column
	@Getter
	@Setter
	private Boolean inCampoFk;

	@Column
	@Getter
	@Setter
	private String txDescricao;

	@Column(nullable = false)
	@Getter
	@Setter
	private LocalDateTime dtRegistro;

	@ManyToOne
	@JoinColumn(name = "tabela_id")
	@Getter
	@Setter
	private Tabela tabela;

	@ManyToMany(fetch = FetchType.LAZY)
	@Getter
	@Setter
	private List<Etiqueta> etiqueta;

//	@OneToMany(fetch = FetchType.LAZY)
//	@JoinColumn(name = "contribuicao_id")
//	@Getter
//	@Setter
//	private List<Contribuicao> contribuicao;
	
}

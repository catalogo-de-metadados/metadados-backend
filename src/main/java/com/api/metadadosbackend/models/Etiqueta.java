package com.api.metadadosbackend.models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_ETIQUETA")

@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor

public class Etiqueta implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Setter
	@Getter
	private UUID id;

	@Column(nullable = false, unique = true, length = 120)
	@Getter
	@Setter
	private String nmEtiqueta;
	
	@Column(nullable = false, length = 1)
	@Getter
	@Setter	
	private String inTipoEtiqueta;

	@Column(nullable = false)
	@Getter
	@Setter
	private LocalDateTime dtRegistro;
//
//	@ManyToOne
//	@Getter
//	@Setter
//	private TipoEtiqueta tipoEtiqueta;

}

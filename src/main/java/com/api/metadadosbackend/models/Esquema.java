package com.api.metadadosbackend.models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_ESQUEMA")
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Esquema implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Setter
	@Getter
	private UUID id;

	@Column(nullable = false, length = 120)
	@Getter
	@Setter
	private String nmEsquema;

	@Column
	@Getter
	@Setter
	private String txDescricao;

	@Column(nullable = false)
	@Getter
	@Setter
	private LocalDateTime dtRegistro;

//	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//	@Getter
//	@Setter
//	private List<Tabela> tabelas = new ArrayList<>();

	@ManyToOne
	@JoinColumn (name="banco_id")
	@Getter
	@Setter
	private Banco banco;

}

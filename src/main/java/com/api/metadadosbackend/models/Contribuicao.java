package com.api.metadadosbackend.models;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_CONTRIBUICAO")
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Contribuicao {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Setter
	@Getter
	private UUID id;

	@Column(nullable = false, columnDefinition="TEXT")
	@Getter
	@Setter
	private String txContribuicao;

	@Column(nullable = false)
	@Getter
	@Setter
	private String usrContribuicao;

	@Column(nullable = false)
	@Getter
	@Setter
	private LocalDateTime dtRegistro;
	
	@ManyToOne
	@JoinColumn (name="atributo_id")
	@Getter
	@Setter
	private Atributo atributo;

}